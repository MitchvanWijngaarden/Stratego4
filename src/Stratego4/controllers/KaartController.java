package Stratego4.controllers;

import Stratego4.models.KaartModel;
import Stratego4.views.KaartPane;

/**
 * Created by Mitch on 5/11/2016.
 */
public class KaartController {
    private KaartModel model;
    private KaartPane view;

    public KaartController(KaartModel model, KaartPane view){
        this.model = model;
        this.view = view;
    }

    public void setX(double x){
        model.setX(model.getX() + x);
    }

    public void setY(double y){
        model.setY(model.getY() + y);
    }

}
