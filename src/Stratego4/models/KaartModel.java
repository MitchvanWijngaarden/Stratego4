package Stratego4.models;

import Stratego4.views.KaartPane;

/**
 * Created by Mitch on 5/11/2016.
 */
public class KaartModel {
    private double x = 250;
    private double y = 250;
    private KaartPane view;

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }

    public void setX(double x){
        this.x = x;
        this.modelChanged();
    }

    public void setY(double y){
        this.y = y;
        this.modelChanged();
    }

    public void registrate(KaartPane view){
        this.view = view;
    }
    private void modelChanged(){
        view.updateView(this);
    }
}
