package Stratego4.app;

import Stratego4.controllers.KaartController;
import Stratego4.models.KaartModel;
import Stratego4.views.KaartPane;
import Stratego4.views.zijkantPane;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

/**
 * Created by Mitch on 5/11/2016.
 */
public class Main extends Application {
    public static void main(String[] args){
        launch(args);
    }

    Scene scene;
    @Override
    public void start(Stage primaryStage) throws Exception {

        Pane pane = new Pane();
        KaartModel kaartModel = new KaartModel();
        KaartPane kp = new KaartPane(kaartModel);

        KaartController controller = new KaartController(kaartModel,kp);

        zijkantPane zijPane = new zijkantPane(controller);

        pane.getChildren().addAll(kp, zijPane);

        scene = new Scene(pane, 1000, 640);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();



    }

}

