package Stratego4.views;

import Stratego4.controllers.KaartController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Created by Mitch on 5/11/2016.
 */
public class zijkantPane extends Pane {
    private Button omhoog, omlaag, links, rechts;
    private VBox vbox;
    private HBox hbox;

    private String[] resolutionList = new String[3];
    private ComboBox<String> resolutionComboBox;


    private KaartController controller;

    public zijkantPane(KaartController controller){
        resolutionList[0] = new String("640 x 480");
        resolutionList[1] = new String("1000 x 640");
        resolutionList[2] = new String("1600 x 1200");

        resolutionComboBox = new ComboBox<String>();
        resolutionComboBox.getItems().addAll(
                resolutionList[0],
                resolutionList[1],
                resolutionList[2]
        );
        resolutionComboBox.setValue(resolutionList[0]);



        resolutionComboBox.setOnAction(e ->{
            String[] resolution = resolutionComboBox.getValue().split(" x ");
            this.getParent().prefWidth(Integer.parseInt(resolution[0]));
            this.getParent().prefHeight(Integer.parseInt(resolution[1]));

        });


        omhoog = new Button("^");
        omlaag = new Button("v");
        links = new Button("<");
        rechts = new Button(">");

        hbox = new HBox(20);
        hbox.getChildren().addAll(links, rechts);
        hbox.setAlignment(Pos.CENTER);
        vbox = new VBox(20);
        vbox.getChildren().addAll(omhoog, hbox, omlaag);
        vbox.setAlignment(Pos.CENTER);



        this.getChildren().add(vbox);
        this.getChildren().add(resolutionComboBox);



        omhoog.setOnMouseClicked(e -> {
            controller.setY(-10);
        });
        omlaag.setOnMouseClicked(e -> {
            controller.setY(10);
        });

        links.setOnMouseClicked(e -> {
            controller.setX(-10);
        });
        rechts.setOnMouseClicked(e -> {
            controller.setX(10);
        });
    }
}
