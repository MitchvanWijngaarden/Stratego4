package Stratego4.views;

import Stratego4.models.KaartModel;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

/**
 * Created by Mitch on 5/11/2016.
 */
public class KaartPane extends Pane {
    private Circle circle;

    public KaartPane(KaartModel model){
        circle = new Circle(100);
        circle.setLayoutX(model.getX());
        circle.setLayoutY(model.getY());
        model.registrate(this);

        this.getChildren().add(circle);
    }

    public void updateView(KaartModel model) {
        circle.setLayoutX(model.getX());
        circle.setLayoutY(model.getY());
    }
}
